<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>

    <fmt:message bundle="${loc}" key="local.add_payment" var="add_payment"/>
    <fmt:message bundle="${loc}" key="local.card_number" var="card_number"/>
    <fmt:message bundle="${loc}" key="local.bank_account_target" var="bank_account_target"/>
    <fmt:message bundle="${loc}" key="local.sum" var="sum"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>
    <fmt:message bundle="${loc}" key="local.incorrect_data" var="incorrect_data"/>
    <fmt:message bundle="${loc}" key="local.not_enough_money" var="not_enough_money"/>
    <fmt:message bundle="${loc}" key="local.bank_account_dont_exist" var="bank_account_dont_exist"/>
    <fmt:message bundle="${loc}" key="local.account_is_blocked" var="account_is_blocked"/>
    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
    <title>${add_payment}</title>

</head>
<body>
<header>
    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="ru"/> <input class="button" type="submit"
                                                              value="${ru_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\add_payment.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="en"/> <input class="button" type="submit"
                                                              value="${en_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\add_payment.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>
    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
        <input class="button" type="submit" value="${my_page}"/>
    </form>
    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="sign_out"/>
        <input class="button" type="submit" value="${sign_out}"/>
    </form>
</header>
<br>
<div  class="input">
    <h1>${add_payment}</h1>
    <c:choose>
        <c:when test="${error eq 'incorrectData'}">
            <h3 class="error">${incorrect_data}</h3>
        </c:when>
        <c:when test="${error eq 'cardDoesNotExist'}">
            <h3 class="error">${bank_account_dont_exist}</h3>
        </c:when>
        <c:when test="${error eq 'bankAccountIsBlocked'}">
            <h3 class="error">${account_is_blocked}</h3>
        </c:when>
    </c:choose>
    <form action="../Controller" method="post">
        <p>${card_number}</p>
        <select id="cards_select" name="cards_select">
            <c:forEach var="card" items="${user_cards}">
                <option>${card.idCard}</option>
            </c:forEach>
        </select>
        <p>${bank_account_target}</p>
        <input type="text" pattern="^[0-9]+$" name="id_bank_account_target"/>
        <c:choose>
            <c:when test="${error eq 'bankAccountDoesNotExist'}">
                <h3 class="error">${bank_account_dont_exist}</h3>
            </c:when>
        </c:choose>
        <p>${sum}</p>
        <c:choose>
            <c:when test="${error eq 'notEnoughMoney'}">
                <h3 class="error">${not_enough_money}</h3>
            </c:when>
        </c:choose>
        <input type="text" pattern="^[0-9]+$" name="sum"/><br><br>
        <input type="hidden" name="command" value="add_payment">
        <input class="button" type="submit" value="${add_payment}"/>
    </form>
</div>
<style>
    @import "style.css";
</style>
</body>
</html>
