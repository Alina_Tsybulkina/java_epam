<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>

    <fmt:message bundle="${loc}" key="local.hello" var="hello"/>
    <fmt:message bundle="${loc}" key="local.accounts" var="accounts"/>
    <fmt:message bundle="${loc}" key="local.cards" var="cards"/>
    <fmt:message bundle="${loc}" key="local.payments" var="payments"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>
    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
    <title>${hello}</title>
</head>
<body>
<header>
    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="ru"/> <input type="submit"
                                                              value="${ru_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\user.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="en"/> <input type="submit"
                                                              value="${en_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\user.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
        <input type="submit" value="${my_page}"/>
    </form>
    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="sign_out"/>
        <input type="submit" value="${sign_out}"/>
    </form>
</header>
<br><br>

<div class="div_left">
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="user_accounts"/>
        <input type="submit" value="${accounts}"/><br><br>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="user_cards"/>
        <input type="submit" value="${cards}"/><br><br>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="user_payments"/>
        <input type="submit" value="${payments}"/><br><br>
    </form>
</div>

<div class="div_center">
    <h1>${hello}, ${user.name} ${user.surname}</h1>
</div>

<style>
    @import "style.css";

</style>
</body>
</html>


