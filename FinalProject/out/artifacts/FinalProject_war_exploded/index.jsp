<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
  <head>
      <fmt:setLocale value="${sessionScope.local}" />
      <fmt:setBundle basename="localization.local" var="loc" />
      <fmt:message bundle="${loc}" key="local.hellomessage" var="hellomessage" />
      <fmt:message bundle="${loc}" key="local.enter" var="enter" />
      <fmt:message bundle="${loc}" key="local.registration" var="registration" />
      <fmt:message bundle="${loc}" key="local.payment_system" var="payment_system" />
      <fmt:message bundle="${loc}" key="local.invalid_login" var="invalid_login" />
      <fmt:message bundle="${loc}" key="local.login" var="login" />
      <fmt:message bundle="${loc}" key="local.password" var="password" />


      <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                   var="ru_button" />
      <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                   var="en_button" />
      <title>${payment_system}</title>
  </head>
  <body>
    <form class="btn" action="Controller" method="post">
        <input type="hidden" name="local" value="ru" /> <input type="submit"
                                                               value="${ru_button}" />
        <input type="hidden" name="page" value="index.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="Controller" method="post">
        <input type="hidden" name="local" value="en" /> <input type="submit"
                                                               value="${en_button}" />
        <input type="hidden" name="page" value="index.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form><br>
<div style="margin-left: 10px">
    <h1>${hellomessage}</h1>
    <c:choose>
        <c:when test="${error eq 'invalidLogin'}">
            <h3 class="error">${invalid_login}</h3>
        </c:when>
    </c:choose>
    <form class="frm" action="Controller" method="post" >
        <input class="field" type="text" name="login" placeholder="${login}">
        <input class="field" type="password" name="password" placeholder="${password}">
        <input class="enter" type="submit" value="${enter}" >
        <input type="hidden" name="command" value="login"/>
    </form>

    <form action="/Controller" method="post">
        <input type="submit" value="${registration}"/>
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\registr.jsp"/>
    </form>
</div>
    <style>
      @import "style.css";
    </style>
  </body>
</html>


