<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>

    <fmt:message bundle="${loc}" key="local.accounts" var="accounts"/>
    <fmt:message bundle="${loc}" key="local.cards" var="cards"/>
    <fmt:message bundle="${loc}" key="local.payments" var="payments"/>

    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
    <fmt:message bundle="${loc}" key="local.id_bank_account" var="id_bank_account"/>
    <fmt:message bundle="${loc}" key="local.balance" var="balance"/>
    <fmt:message bundle="${loc}" key="local.is_blocked" var="is_blocked"/>
    <fmt:message bundle="${loc}" key="local.block" var="block"/>
    <fmt:message bundle="${loc}" key="local.is_not_blocked" var="is_not_blocked"/>
    <fmt:message bundle="${loc}" key="local.is_blocked_true" var="is_blocked_true"/>
    <fmt:message bundle="${loc}" key="local.add_money" var="add_money"/>
    <fmt:message bundle="${loc}" key="local.add_account" var="add_account"/>
    <title>${accounts}</title>

    <style>
        @import "style.css";
    </style>
</head>
<body>
<header>
<form class="btn" action="../Controller" method="post">
    <input type="hidden" name="local" value="ru"/>
    <input class="button" type="submit" value="${ru_button}"/>
    <input type="hidden" name="page" value="WEB-INF\jsp\user_accounts.jsp"/>
    <input type="hidden" name="command" value="local"/>
</form>

<form class="btn" action="../Controller" method="post">
    <input type="hidden" name="local" value="en"/>
    <input class="button" type="submit" value="${en_button}"/>
    <input type="hidden" name="page" value="WEB-INF\jsp\user_accounts.jsp"/>
    <input type="hidden" name="command" value="local"/>
</form>

<form class="btn_right" action="../Controller" method="post">
    <input type="hidden" name="command" value="goto"/>
    <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
    <input class="button" type="submit" value="${my_page}"/>
</form>
<form class="btn_right" action="../Controller" method="post">
    <input type="hidden" name="command" value="sign_out"/>
    <input class="button" type="submit" value="${sign_out}"/>
</form>
</header>
<br><br><br>

<div class="div_left">
    <form class="btn-left" action="../Controller" method="post">
        <input type="hidden" name="command" value="user_accounts"/>
        <input class="button" type="submit" value="${accounts}"/><br><br>
    </form>
    <form class="btn-left" action="../Controller" method="post">
        <input type="hidden" name="command" value="user_cards"/>
        <input class="button" type="submit" value="${cards}"/><br><br>
    </form>
    <form class="btn-left" action="../Controller" method="post">
        <input type="hidden" name="command" value="user_payments"/>
        <input class="button" type="submit" value="${payments}"/><br><br>
    </form>
</div>

<div class="div_center">
    <table>
        <tr>
            <th>${id_bank_account}</th>
            <th>${balance}</th>
            <th>${is_blocked}</th>
            <th>${block}</th>
        </tr>

        <c:forEach var="user_accounts" items="${user_accounts}">
            <tr>
                <td><c:out value="${user_accounts.idBankAccount}"/></td>
                <td><c:out value="${user_accounts.balance}"/></td>
                <td>
                    <c:out value="${user_accounts.isBlocked == true ? is_blocked_true : is_not_blocked}"/>
                </td>
                <td>
                    <form class="form_inline" action="../Controller" method="post">
                        <input type="hidden" name="command" value="block_account"/>
                        <input type="hidden" name="id_bank_account" value="${user_accounts.idBankAccount}"/>
                        <input type="hidden" name="balance" value="${user_accounts.balance}"/>
                        <c:choose>
                            <c:when test="${user_accounts.isBlocked == true}">
                                <input disabled type="submit" value="${block}"/>
                            </c:when>
                            <c:otherwise>
                                <input type="submit" value="${block}"/>
                            </c:otherwise>
                        </c:choose>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<div class="div_right">
    <form class="btn-right" action="../Controller" method="post">
        <input type="hidden" name="command" value="add_account"/>
        <input class="button" type="submit" value="${add_account}"/>
    </form>
    <form class="btn-right" action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\add_money.jsp"/>
        <input class="button" type="submit" value="${add_money}"/>
    </form>
</div>

</body>
</html>
