<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>

    <fmt:message bundle="${loc}" key="local.hello" var="hello"/>
    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
    <fmt:message bundle="${loc}" key="local.as_admin" var="as_admin"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>
    <fmt:message bundle="${loc}" key="local.add_admin" var="add_admin"/>
    <fmt:message bundle="${loc}" key="local.all_accounts" var="all_accounts"/>
    <fmt:message bundle="${loc}" key="local.all_cards" var="all_cards"/>
    <fmt:message bundle="${loc}" key="local.all_payments" var="all_payments"/>
    <fmt:message bundle="${loc}" key="local.all_users" var="all_users"/>
    <fmt:message bundle="${loc}" key="local.delete" var="delete"/>
    <fmt:message bundle="${loc}" key="local.user_id" var="user_id"/>
    <fmt:message bundle="${loc}" key="local.name" var="name" />
    <fmt:message bundle="${loc}" key="local.surname" var="surname" />
    <fmt:message bundle="${loc}" key="local.login" var="login" />
    <fmt:message bundle="${loc}" key="local.is_admin" var="is_admin" />
    <fmt:message bundle="${loc}" key="local.is_admin_true" var="is_admin_true" />
    <fmt:message bundle="${loc}" key="local.is_not_admin" var="is_not_admin" />

    <title>${all_users}</title>
</head>
<body>
<header>
<form class="btn" action="../Controller" method="post">
    <input type="hidden" name="local" value="ru"/> <input class="button" type="submit"
                                                          value="${ru_button}"/>
    <input type="hidden" name="page" value="WEB-INF\jsp\admin\all_users.jsp"/>
    <input type="hidden" name="command" value="local"/>
</form>

<form class="btn" action="../Controller" method="post">
    <input type="hidden" name="local" value="en"/> <input class="button" type="submit"
                                                          value="${en_button}"/>
    <input type="hidden" name="page" value="WEB-INF\jsp\admin\all_users.jsp"/>
    <input type="hidden" name="command" value="local"/>
</form>

<form class="btn_right" action="../Controller" method="post">
    <input type="hidden" name="command" value="goto"/>
    <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
    <input class="button" type="submit" value="${my_page}"/>
</form>
<form class="btn_right" action="../Controller" method="post">
    <input type="hidden" name="command" value="sign_out"/>
    <input class="button" type="submit" value="${sign_out}"/>
</form>
</header>
<br><br>

<div class="div_left">
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\admin\add_admin.jsp"/>
        <input class="button" type="submit" value="${add_admin}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_payments"/>
        <input class="button" type="submit" value="${all_payments}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_accounts"/>
        <input class="button" type="submit" value="${all_accounts}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_cards"/>
        <input class="button" type="submit" value="${all_cards}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_users"/>
        <input class="button" type="submit" value="${all_users}"/>
    </form>
</div>
<div class="div_center">
    <table class="table">
        <tr>
            <th>${user_id}</th>
            <th>${name}</th>
            <th>${surname}</th>
            <th>${login}</th>
            <th>${is_admin}</th>
            <th>${delete}</th>
        </tr>

        <c:forEach var="users" items="${users}">
            <tr>
                <td><c:out value="${users.idUser}"/></td>
                <td><c:out value="${users.name}"/></td>
                <td><c:out value="${users.surname}"/></td>
                <td><c:out value="${users.login}"/></td>
                <td><c:out value="${users.isAdmin == true ? is_admin_true : is_not_admin}"/></td>
                <td>
                    <form>
                        <input type="hidden" name="command" value="delete_user"/>
                        <input type="hidden" name="id_user" value="${users.idUser}"/>
                        <input type="submit" value="${delete}"/>
                    </form>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>


<style>
    @import "style.css";
</style>
</body>
</html>


