<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>

    <fmt:message bundle="${loc}" key="local.hello" var="hello"/>
    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
    <fmt:message bundle="${loc}" key="local.as_admin" var="as_admin"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>
    <fmt:message bundle="${loc}" key="local.add_admin" var="add_admin"/>
    <fmt:message bundle="${loc}" key="local.all_accounts" var="all_accounts"/>
    <fmt:message bundle="${loc}" key="local.all_cards" var="all_cards"/>
    <fmt:message bundle="${loc}" key="local.all_payments" var="all_payments"/>
    <fmt:message bundle="${loc}" key="local.all_users" var="all_users"/>
    <fmt:message bundle="${loc}" key="local.payment_number" var="payment_number"/>
    <fmt:message bundle="${loc}" key="local.card_number" var="card_number"/>
    <fmt:message bundle="${loc}" key="local.bank_account_target" var="bank_account_target"/>
    <fmt:message bundle="${loc}" key="local.sum" var="sum"/>

    <title>${all_payments}</title>
</head>
<body>
<header>
    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="ru"/> <input type="submit"
                                                              value="${ru_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\admin\all_payments.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="en"/> <input type="submit"
                                                              value="${en_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\admin\all_payments.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
        <input type="submit" value="${my_page}"/>
    </form>
    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="sign_out"/>
        <input type="submit" value="${sign_out}"/>
    </form>
</header>
<br><br>

<div class="div_left">
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\admin\add_admin.jsp"/>
        <input class="button" type="submit" value="${add_admin}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_payments"/>
        <input class="button" type="submit" value="${all_payments}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_accounts"/>
        <input class="button" type="submit" value="${all_accounts}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_cards"/>
        <input class="button" type="submit" value="${all_cards}"/>
    </form>
    <form action="../Controller" method="post">
        <input type="hidden" name="command" value="all_users"/>
        <input class="button" type="submit" value="${all_users}"/>
    </form>
</div>
<div class="div_center">
    <table class="table">
        <tr>
            <th>${payment_number}</th>
            <th>${card_number}</th>
            <th>${bank_account_target}</th>
            <th>${sum}</th>
        </tr>

        <c:forEach var="payments" items="${payments}">
            <tr>
                <td><c:out value="${payments.idPayment}"/></td>
                <td><c:out value="${payments.idCard}"/></td>
                <td><c:out value="${payments.idBankAccountTarget}"/></td>
                <td><c:out value="${payments.sum}"/></td>
            </tr>
        </c:forEach>

    </table>
</div>


<style>
    @import "style.css";
</style>
</body>
</html>


