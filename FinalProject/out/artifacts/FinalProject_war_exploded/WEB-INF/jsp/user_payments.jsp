<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="mytag" %>
<html>
<head>
    <title>Payments</title>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>

    <fmt:message bundle="${loc}" key="local.accounts" var="accounts"/>
    <fmt:message bundle="${loc}" key="local.cards" var="cards"/>
    <fmt:message bundle="${loc}" key="local.payments" var="payments"/>
    <fmt:message bundle="${loc}" key="local.payment_number" var="payment_number"/>
    <fmt:message bundle="${loc}" key="local.card_number" var="card_number"/>
    <fmt:message bundle="${loc}" key="local.bank_account_target" var="bank_account_target"/>
    <fmt:message bundle="${loc}" key="local.sum" var="sum"/>
    <fmt:message bundle="${loc}" key="local.add_payment" var="add_payment"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>
    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
</head>
<body>
<header>
    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="ru"/> <input type="submit"
                                                              value="${ru_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\user_payments.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="en"/> <input type="submit"
                                                              value="${en_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\user_payments.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
        <input type="submit" value="${my_page}"/>
    </form>
    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="sign_out"/>
        <input type="submit" value="${sign_out}"/>
    </form>
</header>
<br><br><br>

<div>

    <div class="div_left">
        <form action="../Controller" method="get">
            <input type="hidden" name="command" value="user_accounts"/>
            <input type="submit" value="${accounts}"/><br><br>
        </form>
        <form action="../Controller" method="get">
            <input type="hidden" name="command" value="user_cards"/>
            <input type="submit" value="${cards}"/><br><br>
        </form>
        <form action="../Controller" method="get">
            <input type="hidden" name="command" value="user_payments"/>
            <input type="submit" value="${payments}"/><br><br>
        </form>
    </div>

    <div class="div_center">

        <table class="table">
            <tr>
                <th>${payment_number}</th>
                <th>${card_number}</th>
                <th>${bank_account_target}</th>
                <th>${sum}</th>
            </tr>
            <mytag:payments-output payments="${user_payments}"/>

        </table>
    </div>

    <div class="div_right">
        <form action="../Controller" method="post">
            <input type="hidden" name="command" value="get_cards"/>
            <input type="submit" value="${add_payment}"/>
        </form>

    </div>
</div>
<style>
    @import "style.css";
</style>
</body>
</html>
