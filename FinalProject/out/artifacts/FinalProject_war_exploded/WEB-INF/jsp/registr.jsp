<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>

    <fmt:message bundle="${loc}" key="local.name" var="name"/>
    <fmt:message bundle="${loc}" key="local.surname" var="surname"/>
    <fmt:message bundle="${loc}" key="local.login" var="login"/>
    <fmt:message bundle="${loc}" key="local.password" var="password"/>
    <fmt:message bundle="${loc}" key="local.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="local.incorrect_data" var="incorrect_data"/>
    <fmt:message bundle="${loc}" key="local.login_is_not_available" var="login_is_not_available"/>
    <title>${registration}</title>

</head>
<body>
<header>
    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="ru"/> <input type="submit"
                                                              value="${ru_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\registr.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="en"/> <input type="submit"
                                                              value="${en_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\registr.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>
</header>
<br>
<div class="input">
    <h1>${registration}</h1>
    <c:choose>
        <c:when test="${error eq 'loginIsNotAvailable'}">
            <h3 class="error">${login_is_not_available}</h3>
        </c:when>
        <c:when test="${error eq 'incorrectData'}">
            <h3 class="error">${incorrect_data}</h3>
        </c:when>
    </c:choose>
    <form action="../Controller" method="post">
        <p>${name}</p>
        <input type="text" pattern="[A-Za-zА-Яа-яЁё]{3,}" name="name"/>
        <p>${surname}</p>
        <input type="text" pattern="[A-Za-zА-Яа-яЁё]{3,}" name="surname"/>
        <p>${login}</p>
        <input type="text" pattern="[^\s]{3,}" name="login"/>
        <p>${password}</p>
        <input type="password" pattern="[^\s]{3,}" name="password"/><br><br>
        <input type="hidden" name="command" value="registration">
        <input type="submit" value="${registration}"/>
    </form>

</div>
<style>
    @import "style.css";
</style>


</body>
</html>
