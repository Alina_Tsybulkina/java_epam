<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru"
                 var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en"
                 var="en_button"/>
    <fmt:message bundle="${loc}" key="local.choose_account" var="choose_account"/>
    <fmt:message bundle="${loc}" key="local.submit" var="submit"/>
    <fmt:message bundle="${loc}" key="local.add_card" var="add_card"/>
    <fmt:message bundle="${loc}" key="local.my_page" var="my_page"/>
    <fmt:message bundle="${loc}" key="local.sign_out" var="sign_out"/>
    <fmt:message bundle="${loc}" key="local.bank_account_dont_exist" var="bank_account_dont_exist"/>
    <title>${add_card}</title>
</head>
<body>
<header>
    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="ru"/>
        <input class="button" type="submit" value="${ru_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\add_card.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn" action="../Controller" method="post">
        <input type="hidden" name="local" value="en"/>
        <input class="button" type="submit" value="${en_button}"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\add_card.jsp"/>
        <input type="hidden" name="command" value="local"/>
    </form>

    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="goto"/>
        <input type="hidden" name="page" value="WEB-INF\jsp\mypage.jsp"/>
        <input class="button" type="submit" value="${my_page}"/>
    </form>
    <form class="btn_right" action="../Controller" method="post">
        <input type="hidden" name="command" value="sign_out"/>
        <input class="button" type="submit" value="${sign_out}"/>
    </form>
</header>
<br><br><br>
<div class="input">
    <c:choose>
        <c:when test="${error eq 'bankAccountDoesNotExist'}">
            <h3 class="error">${bank_account_dont_exist}</h3>
        </c:when>
    </c:choose>
    <form action="../Controller" method="post">
        <p>${choose_account}</p>
        <select id="select_account" name="select_account">
            <c:forEach var="user_accounts" items="${user_accounts}">
                <c:choose>
                    <c:when test="${user_accounts.isBlocked == false}">
                        <option>
                                ${user_accounts.idBankAccount}
                        </option>
                    </c:when>
                </c:choose>
            </c:forEach>
        </select><br><br>
        <input type="hidden" name="command" value="add_card"/>
        <input class="button" type="submit" value="${submit}"/>
    </form>
</div>
<style>
    @import "style.css";
</style>
</body>
</html>
