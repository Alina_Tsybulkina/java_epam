package by.epam.at.dao;


import by.epam.at.dao.impl.AccountsDAO;
import by.epam.at.dao.impl.CardsDAO;
import by.epam.at.dao.impl.UserDAO;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.Account;
import by.epam.at.entity.Card;
import by.epam.at.entity.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.HashMap;
import java.util.List;

public class AccountsDAOTest {
    private static AccountsDAO instance;
    private static Account account;
    private static User user;
    private static Card card;

    private static int userKey;
    private static int accountKey;
    private static int cardKey;

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.initPoolData();
        instance = AccountsDAO.getInstance();
        user = new User();
        user.setLogin("test");
        user.setName("test");
        user.setPassword("test");
        user.setSurname("test");
        account = new Account();
        account.setIsBlocked(false);
        account.setBalance(1);
        card = new Card();
        insert();
    }

    private static void insert() throws DaoException {
        userKey = UserDAO.getInstance().insert(user);
        account.setIdUser(userKey);
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException, DaoException {
        UserDAO.getInstance().delete(userKey);
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.dispose();
    }

    @Test
    public void testSelectOne() throws Exception {
        Account result;
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        result = (Account) instance.selectOne(account);
        Assert.assertEquals(account, result);
        instance.delete(accountKey);
    }

    @Test
    public void testSelectByUserID() throws Exception {
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        List<Account> accounts = instance.selectByUserID(userKey);
        if (!accounts.contains(account)) {
            Assert.fail();
        }
        instance.delete(accountKey);
    }

    @Test
    public void testSelectAll() throws Exception {
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        List<Account> accounts = instance.selectAll();
        if (!accounts.contains(account)) {
            Assert.fail();
        }
        instance.delete(accountKey);
    }

    @Test
    public void testSelect() throws Exception {
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        card.setIdBankAccount(accountKey);
        cardKey = CardsDAO.getInstance().insert(card);
        card.setIdCard(cardKey);
        HashMap<String, String> map = new HashMap<>();
        map.put(CardsDAO.CardsCols.ID_CARD, String.valueOf(cardKey));
        map.put(UserDAO.UserCols.ID_USER, String.valueOf(userKey));
        List<Account> accounts = instance.select(map);
        if (! accounts.contains(account))
        {
            Assert.fail();
        }
        CardsDAO.getInstance().delete(cardKey);
        instance.delete(accountKey);
    }

    @Test
    public void testInsert() throws Exception {
        Account result;
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        result = (Account) instance.selectOne(account);
        Assert.assertEquals(account, result);
        instance.delete(accountKey);
    }

    @Test
    public void testUpdate() throws Exception {
        Account result;
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        account.setIsBlocked(true);
        instance.update(account);
        result = (Account) instance.selectOne(account);
        Assert.assertEquals(account, result);
        instance.delete(accountKey);
    }

    @Test
    public void testDelete() throws Exception
    {
        Account result;
        accountKey = instance.insert(account);
        account.setIdBankAccount(accountKey);
        result = (Account) instance.selectOne(account);
        Assert.assertEquals(account, result);
        instance.delete(accountKey);
        result = (Account) instance.selectOne(account);
        if (result != null)
        {
            Assert.fail();
        }
    }
}