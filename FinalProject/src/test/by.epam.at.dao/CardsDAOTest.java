package by.epam.at.dao;

import by.epam.at.dao.impl.AccountsDAO;
import by.epam.at.dao.impl.CardsDAO;
import by.epam.at.dao.impl.UserDAO;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.Account;
import by.epam.at.entity.Card;
import by.epam.at.entity.User;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.List;
import org.junit.*;

public class CardsDAOTest {
    private static CardsDAO instance;
    private static Account account;
    private static Card card;
    private static User user;

    private static int userKey;
    private static int accountKey;
    private static int cardKey;

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.initPoolData();
        instance = CardsDAO.getInstance();
        user = new User();
        user.setLogin("test");
        user.setName("test");
        user.setPassword("test");
        user.setSurname("test");
        account = new Account();
        account.setIsBlocked(false);
        account.setBalance(1);
        card = new Card();
        insert();
    }

    private static void insert() throws DaoException {
        userKey = UserDAO.getInstance().insert(user);
        account.setIdUser(userKey);
        accountKey = AccountsDAO.getInstance().insert(account);
        card.setIdBankAccount(accountKey);
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException, DaoException {
        AccountsDAO.getInstance().delete(accountKey);
        UserDAO.getInstance().delete(userKey);
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.dispose();
    }

    @Test
    public void testSelectByUserID() throws Exception {
        cardKey = instance.insert(card);
        card.setIdCard(cardKey);
        List<Card> cardList = instance.selectByUserID(userKey);
        if (!cardList.contains(card))
        {
            Assert.fail();
        }
        instance.delete(cardKey);
    }

    @Test
    public void testSelectAll() throws Exception {
        cardKey = instance.insert(card);
        card.setIdCard(cardKey);
        List<Card> cardList = instance.selectAll();
        if (!cardList.contains(card))
        {
            Assert.fail();
        }
        instance.delete(cardKey);
    }

    @Test
    public void testSelectOne() throws Exception
    {
        Card result;
        cardKey = instance.insert(card);
        card.setIdCard(cardKey);
        result = (Card)instance.selectOne(card);
        Assert.assertEquals(card, result);
        instance.delete(cardKey);
    }

    @Test
    public void testInsert() throws Exception {
        Card result;
        cardKey = instance.insert(card);
        card.setIdCard(cardKey);
        result = (Card)instance.selectOne(card);
        Assert.assertEquals(card, result);
        instance.delete(cardKey);
    }

    @Test
    public void testDelete() throws Exception {
        Card result;
        cardKey = instance.insert(card);
        card.setIdCard(cardKey);
        result = (Card)instance.selectOne(card);
        Assert.assertEquals(card, result);
        instance.delete(cardKey);
        result = (Card)instance.selectOne(card);
        if (result != null)
        {
            Assert.fail();
        }
    }
}