package by.epam.at.dao;

import by.epam.at.dao.impl.UserDAO;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.HashMap;
import java.util.List;
import static org.junit.Assert.*;

public class UserDAOTest {
    private static UserDAO instance;
    private static User user;
    private static int userKey;

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.initPoolData();
        instance = UserDAO.getInstance();
        user = new User();
        user.setLogin("test");
        user.setName("test");
        user.setPassword("test");
        user.setSurname("test");
    }

    @Test
    public void testSelectOne() throws Exception {
        userKey = instance.insert(user);
        user.setIdUser(userKey);
        User result = (User) instance.selectOne(user);
        Assert.assertEquals(user, result);
        instance.delete(userKey);
    }

    @Test
    public void testInsert() throws Exception {
        userKey = instance.insert(user);
        user.setIdUser(userKey);
        User result = (User) instance.selectOne(user);
        Assert.assertEquals(user, result);
        instance.delete(userKey);
    }

    @Test
    public void testSelectAll() throws Exception {
        userKey = instance.insert(user);
        user.setIdUser(userKey);
        List<User> result = instance.selectAll();
        if (! result.contains(user))
        {
            fail();
        }
        instance.delete(userKey);
    }

    @Test
    public void testSelect() throws Exception {
        userKey = instance.insert(user);
        user.setIdUser(userKey);
        HashMap<String, String> map = new HashMap<>();
        map.put(UserDAO.UserCols.LOGIN, user.getLogin());
        List<User> result = instance.select(map);
        if (! result.contains(user))
        {
            fail();
        }
        instance.delete(userKey);
    }

    @Test
    public void testUpdate() throws Exception {
        userKey = instance.insert(user);
        user.setIdUser(userKey);
        user.setName("new");
        instance.update(user);
        User result = (User) instance.selectOne(user);
        Assert.assertEquals(user, result);
        instance.delete(userKey);
    }

    @Test
    public void testDelete() throws Exception {
        User result;
        userKey = instance.insert(user);
        user.setIdUser(userKey);
        result = (User) instance.selectOne(user);
        Assert.assertEquals(user, result);
        instance.delete(userKey);
        result = (User) instance.selectOne(user);
        if (result != null)
        {
            Assert.fail();
        }
    }
}