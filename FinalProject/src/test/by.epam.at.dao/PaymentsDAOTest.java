package by.epam.at.dao;

import by.epam.at.dao.impl.AccountsDAO;
import by.epam.at.dao.impl.CardsDAO;
import by.epam.at.dao.impl.PaymentsDAO;
import by.epam.at.dao.impl.UserDAO;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.Account;
import by.epam.at.entity.Card;
import by.epam.at.entity.Payment;
import by.epam.at.entity.User;
import org.junit.*;

import java.util.List;


public class PaymentsDAOTest {

    private static PaymentsDAO instance;
    private static Account account;
    private static Card card;
    private static Payment payment;
    private static User user;

    private static int userKey;
    private static int accountKey;
    private static int cardKey;
    private static int paymentKey;

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.initPoolData();
        instance = PaymentsDAO.getInstance();
        user = new User();
        user.setLogin("test");
        user.setName("test");
        user.setPassword("test");
        user.setSurname("test");
        account = new Account();
        account.setIsBlocked(false);
        account.setBalance(1);
        card = new Card();
        payment = new Payment();
        payment.setSum(1);
        insert();
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException, DaoException {
        CardsDAO.getInstance().delete(cardKey);
        AccountsDAO.getInstance().delete(accountKey);
        UserDAO.getInstance().delete(userKey);
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.dispose();
    }

    private static void insert() throws DaoException {
        userKey = UserDAO.getInstance().insert(user);
        account.setIdUser(userKey);
        accountKey = AccountsDAO.getInstance().insert(account);
        card.setIdBankAccount(accountKey);
        cardKey = CardsDAO.getInstance().insert(card);
        payment.setIdCard(cardKey);
        payment.setIdBankAccountTarget(accountKey);
    }

    @Test
    public void testDelete() throws DaoException
    {
        Payment result;
        paymentKey = instance.insert(payment);
        payment.setIdPayment(paymentKey);
        result = (Payment)instance.selectOne(payment);

        Assert.assertEquals(payment, result);
        instance.delete(paymentKey);
        result = (Payment)instance.selectOne(payment);
        if (result != null)
        {
            Assert.fail();
        }
    }

    @Test
    public void testSelectByUserID() throws Exception {
        paymentKey = instance.insert(payment);
        payment.setIdPayment(paymentKey);
        List<Payment> paymentList = instance.selectByUserID(userKey);
        if (!paymentList.contains(payment))
        {
            Assert.fail();
        }
        instance.delete(paymentKey);
    }

    @Test
    public void testSelectOne() throws Exception
    {
        Payment result;
        paymentKey = instance.insert(payment);
        payment.setIdPayment(paymentKey);
        result = (Payment)instance.selectOne(payment);
        Assert.assertEquals(payment, result);
        instance.delete(paymentKey);
    }

    @Test
    public void testInsert() throws Exception {
        Payment result;
        paymentKey = instance.insert(payment);
        payment.setIdPayment(paymentKey);
        result = (Payment)instance.selectOne(payment);
        Assert.assertEquals(payment, result);
        instance.delete(paymentKey);
    }

    @Test
    public void testSelectAll() throws Exception {
        paymentKey = instance.insert(payment);
        payment.setIdPayment(paymentKey);
        List<Payment> paymentList = instance.selectAll();
        if (!paymentList.contains(payment))
        {
            Assert.fail();
        }
        instance.delete(paymentKey);
    }

}