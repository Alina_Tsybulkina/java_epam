package by.epam.at.listener;

import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import org.apache.log4j.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ServletListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ServletListener.class);

    /**
     * Method which called when servlet context created.
     * Initialises connection pool.
     * @param servletContextEvent - this is the event class for notifications
     *                            about changes to the servlet context of a web application.
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connectionPool.initPoolData();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Connection pool is not initialized!");
        }
    }

    /**
     * Method which called when servlet context deleted.
     * Disposes connection pool.
     * @param servletContextEvent - this is the event class for notifications
     *                            about changes to the servlet context of a web application.
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connectionPool.dispose();
        } catch (ConnectionPoolException e) {
            LOGGER.error(e);
        }

    }
}
