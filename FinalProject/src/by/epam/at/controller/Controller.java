package by.epam.at.controller;

import by.epam.at.controller.command.*;
import org.apache.log4j.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class designed to handle requests from the client and return the result.
 * Extends {@code HttpServlet}.
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * The method is processing request from client and calls the command depending on parameter {@code command}.
     * Then forwards a request from a servlet to another resource.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @param response - object of class {@code HttpServletResponse}, stores an information from server.
     * @throws ServletException - exception a servlet can throw if {@code RequestDispatcher} is used improperly.
     * @throws IOException - exception a servlet can throw if {@code RequestDispatcher} is used improperly.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        String commandName;
        Command command;
        try {
            commandName = request.getParameter(ParameterName.COMMAND_NAME);
            command = CommandHelper.getInstance().getCommand(commandName);
            page = command.execute(request);
        } catch (CommandException e) {
            page = JspPageName.ERROR_PAGE;
            LOGGER.error(e);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(page);

        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }
}
