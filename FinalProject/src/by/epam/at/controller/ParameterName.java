package by.epam.at.controller;

/**
 * Stores names of request parameters.
 */
public final class ParameterName {


    private ParameterName() {
    }

    public static final String ALL_PAYMENTS = "payments";
    public static final String ALL_CARDS = "cards";
    public static final String ALL_ACCOUNTS = "accounts";
    public static final String ALL_USERS = "users";
    public static final String CARD_ID = "id_card";
    public static final String BALANCE = "balance";
    public static final String COMMAND_NAME = "command";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String LOCAL = "local";
    public static final String PAGE = "page";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String USER_ID = "id_user";
    public static final String USER_PAYMENTS = "user_payments";
    public static final String BANK_ACCOUNT_TARGET = "id_bank_account_target";
    public static final String SUM = "sum";
    public static final String CARDS_SELECT = "cards_select";
    public static final String USER_ACCOUNTS = "user_accounts";
    public static final String USER_CARDS = "user_cards";
    public static final String USER = "user";
    public static final String ID_BANK_ACCOUNT = "id_bank_account";
    public static final String SELECT_ACCOUNT = "select_account";
    public static final String ERROR = "error";
}
