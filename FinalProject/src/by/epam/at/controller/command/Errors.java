package by.epam.at.controller.command;

/**
 * Stores names of error parameters.
 */
public final class Errors {
    public static final String LOGIN_IS_NOT_AVAILABLE = "loginIsNotAvailable";
    public static final String INCORRECT_DATA = "incorrectData";
    public static final String BANK_ACCOUNT_DOES_NOT_EXIST = "bankAccountDoesNotExist";
    public static final String INVALID_LOGIN = "invalidLogin";
    public static final String CARD_DOES_NOT_EXIST = "cardDoesNotExist";
    public static final String NOT_ENOUGH_MONEY = "notEnoughMoney";
    public static final String BANK_ACCOUNT_IS_BLOCKED = "bankAccountIsBlocked";
}
