package by.epam.at.controller.command;

import by.epam.at.controller.command.impl.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class which stores a map with commands objects. Singleton.
 */
public final class CommandHelper {

    private static final CommandHelper instance = new CommandHelper();
    private Map<CommandNames, Command> commands = new HashMap<>();

    /**
     * Adds commands objects to map.
     */
    public CommandHelper()
    {
        commands.put(CommandNames.ADD_ACCOUNT, new AddAccountCommand());
        commands.put(CommandNames.ADD_CARD, new AddCardCommand());
        commands.put(CommandNames.ADD_MONEY, new AddMoneyCommand());
        commands.put(CommandNames.SIGN_OUT, new SignOutCommand());
        commands.put(CommandNames.GET_ACCOUNTS, new GetAccountsCommand());
        commands.put(CommandNames.DELETE_CARD, new DeleteCardsCommand());
        commands.put(CommandNames.LOGIN, new LoginCommand());
        commands.put(CommandNames.LOCAL, new LocalCommand());
        commands.put(CommandNames.REGISTRATION, new RegistrationCommand());
        commands.put(CommandNames.USER_ACCOUNTS, new UserAccountsCommand());
        commands.put(CommandNames.USER_CARDS, new UserCardsCommand());
        commands.put(CommandNames.USER_PAYMENTS, new UserPaymentsCommand());
        commands.put(CommandNames.ADD_PAYMENT, new AddPaymentCommand());
        commands.put(CommandNames.GOTO, new GotoCommand());
        commands.put(CommandNames.DELETE_USER, new DeleteUserCommand());
        commands.put(CommandNames.ADD_ADMIN, new AddAdminCommand());
        commands.put(CommandNames.ALL_ACCOUNTS, new AllAccountsCommand());
        commands.put(CommandNames.ALL_CARDS, new AllCardsCommand());
        commands.put(CommandNames.ALL_PAYMENTS, new AllPaymentsCommand());
        commands.put(CommandNames.ALL_USERS, new AllUsersCommand());
        commands.put(CommandNames.GET_CARDS, new GetCardsCommand());
        commands.put(CommandNames.UPDATE_PAGE, new UpdatePageCommand());
        commands.put(CommandNames.UNBLOCK_ACCOUNT, new UnblockAccountCommand());
        commands.put(CommandNames.BLOCK_ACCOUNT, new BlockAccountCommand());
        commands.put(CommandNames.NO_SUCH_COMMAND, new NoSuchCommand());
    }

    public static CommandHelper getInstance()
    {
        return instance;
    }

    /**
     * Method which returns object of command from map, depending on parameter.
     * @param commandName - value of parameter {@code command} from request.
     * @return - command object.
     */
    public Command getCommand(String commandName)
    {
        CommandNames name = CommandNames.valueOf(commandName.toUpperCase());
        Command command;
        if (name != null)
        {
            command = commands.get(name);
        }
        else
        {
            command = commands.get(CommandNames.NO_SUCH_COMMAND);
        }
        return command;
    }
}
