package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.controller.command.Errors;
import by.epam.at.entity.User;
import by.epam.at.service.UserService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class RegistrationCommand implements Command {

    /**
     * Adds user. If data is incorrect set attribute error to request scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        User user = new User();
        if (!validate(user, request))
        {
            request.setAttribute(ParameterName.ERROR, Errors.INCORRECT_DATA);
            return JspPageName.REGISTRATION_PAGE;
        }
        try {
            if (!checkLoginIsAvailable(request)) {
                request.setAttribute(ParameterName.ERROR, Errors.LOGIN_IS_NOT_AVAILABLE);
                return JspPageName.REGISTRATION_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException(e);
        }
        user.setIsAdmin(false);

        try {
            UserService.getInstance().registerUser(user);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return JspPageName.INDEX_PAGE;
    }


    private boolean checkLoginIsAvailable(HttpServletRequest request) throws ServiceException {
        Map<String, String> map = new HashMap<>();
        map.put(ParameterName.LOGIN, request.getParameter(ParameterName.LOGIN));
        if (UserService.getInstance().getByLogin(map).isEmpty())
        {
            return true;
        }
        return false;
    }

    /**
     * Checks if data is not empty and set fields of object {@code user}.
     * @param user object of class {@code User}.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if data is correct, false if not.
     */
    private boolean validate(User user, HttpServletRequest request)
    {
        if (request.getParameter(ParameterName.NAME).length() < 3 || request.getParameter(ParameterName.SURNAME).length() < 3 ||
                request.getParameter(ParameterName.LOGIN).length() < 3 || request.getParameter(ParameterName.PASSWORD).length() < 3)
        {
            return false;
        }
        else {
            user.setName(request.getParameter(ParameterName.NAME));
            user.setSurname(request.getParameter(ParameterName.SURNAME));
            user.setLogin(request.getParameter(ParameterName.LOGIN));
            user.setPassword(String.valueOf(request.getParameter(ParameterName.PASSWORD).hashCode()));
            return true;
        }
    }
}
