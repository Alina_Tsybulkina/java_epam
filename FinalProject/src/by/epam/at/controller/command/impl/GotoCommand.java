package by.epam.at.controller.command.impl;

import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import javax.servlet.http.HttpServletRequest;

public class GotoCommand implements Command {

    /**
     * Returns the name of jsp page from request scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     */
    @Override
    public String execute(HttpServletRequest request) {
        return request.getParameter(ParameterName.PAGE);
    }
}
