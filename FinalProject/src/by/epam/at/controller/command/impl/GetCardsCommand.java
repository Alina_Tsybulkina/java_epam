package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Card;
import by.epam.at.service.CardsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetCardsCommand implements Command {

    /**
     * Adds list of all cards of current user to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if getting cards was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try{
            List<Card> cardList = CardsService.getInstance().getCardsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
            request.getSession(false).setAttribute(ParameterName.USER_CARDS, cardList);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.ADD_PAYMENT;
    }
}
