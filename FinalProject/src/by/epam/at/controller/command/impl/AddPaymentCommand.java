package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.controller.command.Errors;
import by.epam.at.entity.Account;
import by.epam.at.entity.Payment;
import by.epam.at.service.AccountsService;
import by.epam.at.service.PaymentsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

public class AddPaymentCommand implements Command {

    /**
     * Method which adds payment.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Payment payment = new Payment();
        try {
            if (!validate(payment, request)) {
                request.setAttribute(ParameterName.ERROR, Errors.INCORRECT_DATA);
                return JspPageName.ADD_PAYMENT;
            } else {
                if (checkAccountSource(payment, request) && checkAccountTarget(payment, request)) {
                    PaymentsService.getInstance().addPayment(payment);
                    List<Payment> payments = PaymentsService.getInstance().getPaymentsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
                    request.getSession(false).setAttribute(ParameterName.USER_PAYMENTS, payments);
                }
                else {
                    return JspPageName.ADD_PAYMENT;
                }
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return JspPageName.USER_PAYMENTS;
    }

    /**
     * Validates data from client.
     * @param payment - object of class {@code Payment}.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if data is correct.
     */
    private boolean validate(Payment payment, HttpServletRequest request) {
        if (request.getParameter(ParameterName.CARDS_SELECT).isEmpty() || request.getParameter(ParameterName.BANK_ACCOUNT_TARGET).isEmpty()
                || request.getParameter(ParameterName.SUM).isEmpty()) {
           return false;

        } else {
            try {
                payment.setIdCard(Integer.parseInt(request.getParameter(ParameterName.CARDS_SELECT)));
                payment.setIdBankAccountTarget(Integer.parseInt(request.getParameter(ParameterName.BANK_ACCOUNT_TARGET)));
                payment.setSum(Integer.parseInt(request.getParameter(ParameterName.SUM)));
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    }

    /**
     * Checks target bank account.
     * @param payment - object of class {@code Payment}.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if target account exists and not blocked.
     * @throws ServiceException if method {@code getAccount(Account account)} throw exception.
     */
    private boolean checkAccountTarget(Payment payment, HttpServletRequest request) throws ServiceException {
        Account account = new Account();
        account.setIdBankAccount(payment.getIdBankAccountTarget());
        Account accountResult = AccountsService.getInstance().getAccount(account);
        if (accountResult == null)
        {
            request.setAttribute(ParameterName.ERROR, Errors.BANK_ACCOUNT_DOES_NOT_EXIST);
            return false;
        }
        if (accountResult.getIsBlocked())
        {
            request.setAttribute(ParameterName.ERROR, Errors.BANK_ACCOUNT_DOES_NOT_EXIST);
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Checks sourse bank account.
     * @param payment - object of class {@code Payment}.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if source account exists, not blocked and balance is enough to make payment.
     * @throws ServiceException if method {@code getAccount(Account account)} throw exception.
     */
    private boolean checkAccountSource(Payment payment, HttpServletRequest request) throws ServiceException {
        HashMap<String, String> map = new HashMap<>(2);
        map.put(ParameterName.CARD_ID, String.valueOf(payment.getIdCard()));
        map.put(ParameterName.USER_ID, String.valueOf(request.getSession(false).getAttribute(ParameterName.USER_ID)));
        Account account = AccountsService.getInstance().getAccountByCard(map);
        if (account == null)
        {
            request.setAttribute(ParameterName.ERROR, Errors.CARD_DOES_NOT_EXIST);
            return false;
        }
        if (account.getIsBlocked())
        {
            request.setAttribute(ParameterName.ERROR, Errors.BANK_ACCOUNT_IS_BLOCKED);
            return false;
        }
        if (account.getBalance() < payment.getSum())
        {
            request.setAttribute(ParameterName.ERROR, Errors.NOT_ENOUGH_MONEY);
            return false;
        }
        return true;
    }
}
