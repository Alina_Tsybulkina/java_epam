package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Card;
import by.epam.at.entity.User;
import by.epam.at.service.CardsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class DeleteCardsCommand implements Command {

    /**
     * Delete card of current user and adds list of user's cards to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if deleting was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        List<Card> cards;
        try {
            CardsService.getInstance().deleteCard(Integer.parseInt(request.getParameter(ParameterName.CARD_ID)));
            if (!((User) request.getSession(false).getAttribute(ParameterName.USER)).getIsAdmin()) {
                cards = CardsService.getInstance().getCardsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
                request.getSession(false).setAttribute(ParameterName.USER_CARDS, cards);
                return JspPageName.USER_CARDS;
            } else {
                cards = CardsService.getInstance().getAllCards();
                request.getSession(false).setAttribute(ParameterName.ALL_CARDS, cards);
                return JspPageName.ALL_CARDS;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
