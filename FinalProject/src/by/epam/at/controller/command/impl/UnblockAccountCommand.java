package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Account;
import by.epam.at.entity.User;
import by.epam.at.service.AccountsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class UnblockAccountCommand implements Command {

    /**
     * Unblocks selected account.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if unblocking was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        Account account = new Account();
        account.setIdBankAccount(Integer.parseInt(request.getParameter(ParameterName.ID_BANK_ACCOUNT)));
        account.setIsBlocked(false);
        account.setBalance(Integer.parseInt(request.getParameter(ParameterName.BALANCE)));

        try {
            AccountsService.getInstance().updateAccount(account);
            if(!((User)request.getSession(false).getAttribute(ParameterName.USER)).getIsAdmin()) {
                List<Account> accounts = AccountsService.getInstance().getAccountsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
                request.getSession(false).setAttribute(ParameterName.USER_ACCOUNTS, accounts);
                return JspPageName.USER_ACCOUNTS;
            }
            else {
                List<Account> accounts = AccountsService.getInstance().getAllAccounts();
                request.getSession(false).setAttribute(ParameterName.ALL_ACCOUNTS, accounts);
                return JspPageName.ALL_ACCOUNTS;
            }
        }
        catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
