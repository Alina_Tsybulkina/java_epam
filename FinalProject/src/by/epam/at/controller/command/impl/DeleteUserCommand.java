package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.User;
import by.epam.at.service.UserService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class DeleteUserCommand implements Command {

    /**
     * Deletes selected user and adds the list of all users to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if deleting was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        List<User> users;
        try {
            UserService.getInstance().deleteUser(Integer.parseInt(request.getParameter(ParameterName.USER_ID)));
            users = UserService.getInstance().selectAllUsers();
            request.getSession(false).setAttribute(ParameterName.ALL_USERS, users);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.ALL_USERS;
    }
}
