package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.controller.command.Errors;
import by.epam.at.entity.Account;
import by.epam.at.entity.Card;
import by.epam.at.service.CardsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AddCardCommand implements Command {

    /**
     * Method which adds card to current user.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try{
            Card card = new Card();
            if (checkAccountNumber(request)) {
                card.setIdBankAccount(Integer.parseInt(request.getParameter(ParameterName.SELECT_ACCOUNT)));
                CardsService.getInstance().addCard(card);
            }
            else {
                request.setAttribute(ParameterName.ERROR, Errors.BANK_ACCOUNT_DOES_NOT_EXIST);
                return JspPageName.ADD_CARD;
            }
            List<Card> cardList = CardsService.getInstance().getCardsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
            request.getSession(false).setAttribute(ParameterName.USER_CARDS, cardList);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.USER_CARDS;
    }

    /**
     * Check if account number belongs to current user and not blocked.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if account belongs to current user and not blocked.
     */
    private boolean checkAccountNumber(HttpServletRequest request)
    {
        List<Account> accounts = (List<Account>) request.getSession(false).getAttribute(ParameterName.USER_ACCOUNTS);
        for (Account account : accounts){
            if (account.getIdBankAccount() == Integer.parseInt(request.getParameter(ParameterName.SELECT_ACCOUNT)))
            {
                if (account.getIsBlocked() == true)
                {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        return false;
    }
}
