package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Payment;
import by.epam.at.service.PaymentsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class UserPaymentsCommand implements Command {

    /**
     * Adds list of user's payments to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of page with user's payments.
     * @throws CommandException if getting payments was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try{
            List<Payment> payments = PaymentsService.getInstance().getPaymentsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
            request.getSession(false).setAttribute(ParameterName.USER_PAYMENTS, payments);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.USER_PAYMENTS;
    }
}
