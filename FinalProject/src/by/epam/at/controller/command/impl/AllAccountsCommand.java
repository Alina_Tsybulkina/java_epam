package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Account;
import by.epam.at.service.AccountsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AllAccountsCommand implements Command {

    /**
     * Adds list of all accounts to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        List<Account> accounts;
        try{
            accounts = AccountsService.getInstance().getAllAccounts();
            request.getSession(false).setAttribute(ParameterName.ALL_ACCOUNTS, accounts);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.ALL_ACCOUNTS;
    }
}
