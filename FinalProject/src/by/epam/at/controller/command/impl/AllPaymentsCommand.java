package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Payment;
import by.epam.at.service.PaymentsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AllPaymentsCommand implements Command {

    /**
     * Adds list of all payments to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        List<Payment> payments;
        try{
            payments = PaymentsService.getInstance().allPayments();
            request.getSession(false).setAttribute(ParameterName.ALL_PAYMENTS, payments);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }

        return JspPageName.ALL_PAYMENTS;
    }
}
