package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.controller.command.Errors;
import by.epam.at.entity.User;
import by.epam.at.service.UserService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class LoginCommand implements Command {

    /**
     * Returns user with login and password from request and set to session scope, if there is no such user returns login page.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page, admin page if user is admin, and user page for user, if login or password was incorrect, returns start page.
     * @throws CommandException if getting user was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        User user = new User();
        user.setLogin(request.getParameter(ParameterName.LOGIN));
        user.setPassword(String.valueOf((request.getParameter(ParameterName.PASSWORD)).hashCode()));
        User result;
        try {
            result = UserService.getInstance().getUser(user);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return processingResult(request, user, result);
    }

    private String processingResult(HttpServletRequest request, User currentUser, User result)
    {
        if (result == null)
        {
            request.setAttribute(ParameterName.ERROR, Errors.INVALID_LOGIN);
            return JspPageName.INDEX_PAGE;
        }
        if (result.getIsAdmin())
        {
            HttpSession session;
            if ((session = request.getSession(false)) == null)
            {
                session = request.getSession(true);
            }
            session.setAttribute(ParameterName.USER, currentUser);
            return JspPageName.ADMIN_PAGE;
        }
        else {
            HttpSession session;
            if ((session = request.getSession(false)) == null)
            {
                session = request.getSession(true);
            }
            session.setAttribute(ParameterName.USER, currentUser);
            session.setAttribute(ParameterName.USER_ID, currentUser.getIdUser());
            return JspPageName.USER_PAGE;
        }
    }

}
