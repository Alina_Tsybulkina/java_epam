package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Card;
import by.epam.at.service.CardsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AllCardsCommand implements Command {

    /**
     * Adds list of all cards to session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        List<Card> cards;
        try{
            cards = CardsService.getInstance().getAllCards();
            request.getSession(false).setAttribute(ParameterName.ALL_CARDS, cards);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.ALL_CARDS;
    }
}
