package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.controller.command.Errors;
import by.epam.at.entity.Account;
import by.epam.at.service.AccountsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AddMoneyCommand implements Command {

    /**
     * Method which adds money to bank account.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            Account account = null;
            for (Account acc : (List<Account>)request.getSession(false).getAttribute(ParameterName.USER_ACCOUNTS))
            {
                if (acc.getIdBankAccount() == Integer.parseInt(request.getParameter(ParameterName.SELECT_ACCOUNT)))
                {
                    account = acc;
                    break;
                }
            }
            if (validate(request, account))
            {
                AccountsService.getInstance().updateAccount(account);
            }
            else {
                request.setAttribute(ParameterName.ERROR, Errors.INCORRECT_DATA);
                return JspPageName.ADD_MONEY;
            }
            List<Account> accounts = AccountsService.getInstance().getAccountsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
            request.getSession(false).setAttribute(ParameterName.USER_ACCOUNTS, accounts);
        }
        catch (ServiceException e)
        {
            throw new CommandException(e);
        }
        return JspPageName.USER_ACCOUNTS;
    }

    /**
     * Validating parameter {@code sum} and adding money to bank account.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @param account - object of class {@code Account}.
     * @return true if parameter is correct.
     */
    private boolean validate(HttpServletRequest request, Account account)
    {
        if (request.getParameter(ParameterName.SUM).isEmpty() || account == null)
        {
            return false;
        }
        try{
            account.setBalance(account.getBalance() + Integer.parseInt(request.getParameter(ParameterName.SUM)));
        }
        catch (NumberFormatException e)
        {
            return false;
        }
        return true;
    }
}
