package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.controller.command.Errors;
import by.epam.at.entity.User;
import by.epam.at.service.UserService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class UpdatePageCommand implements Command {

    /**
     * Method which updates user's data.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if updating was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        User user = (User) request.getSession(false).getAttribute(ParameterName.USER);
        String login = user.getLogin();
        if (!validate(request, user)) {
            request.setAttribute(ParameterName.ERROR, Errors.INCORRECT_DATA);
            return JspPageName.MY_PAGE;
        }
        try {
            if (!checkLoginIsAvailable(request, login))
            {
                request.setAttribute(ParameterName.ERROR, Errors.LOGIN_IS_NOT_AVAILABLE);
                return JspPageName.MY_PAGE;
            }
            UserService.getInstance().updateUser(user);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        if (user.getIsAdmin())
        {
            return JspPageName.ADMIN_PAGE;
        }
        else {
            return JspPageName.USER_PAGE;
        }
    }

    /**
     * Method checks if login is available.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if login is available, false if not.
     * @throws ServiceException - can be thrown in service methods.
     */
    private boolean checkLoginIsAvailable(HttpServletRequest request, String currentLogin) throws ServiceException {
        if(request.getParameter(ParameterName.LOGIN).equals(currentLogin)) {
            return true;
        }
        Map<String, String> map = new HashMap<>();
        map.put(ParameterName.LOGIN, request.getParameter(ParameterName.LOGIN));
        if (UserService.getInstance().getByLogin(map).isEmpty())
        {
            return true;
        }
        return false;
    }

    /**
     * Method which check parameters and sets fields of user object.
     * @param user - object of class {@code User}.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return true if parameters are correct and false if not.
     */
    private boolean validate(HttpServletRequest request, User user) {
        if (!(request.getParameter(ParameterName.NAME).isEmpty() && request.getParameter(ParameterName.SURNAME).isEmpty() &&
                request.getParameter(ParameterName.LOGIN).isEmpty() && request.getParameter(ParameterName.PASSWORD).isEmpty())) {

            user.setName(request.getParameter(ParameterName.NAME));
            user.setSurname(request.getParameter(ParameterName.SURNAME));
            user.setLogin(request.getParameter(ParameterName.LOGIN));
            if (!user.getPassword().equals(request.getParameter(ParameterName.PASSWORD))) {
                user.setPassword(String.valueOf((request.getParameter(ParameterName.PASSWORD)).hashCode()));
            }
            return true;
        }
        return false;
    }
}
