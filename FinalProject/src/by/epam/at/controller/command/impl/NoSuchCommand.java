package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.command.Command;
import javax.servlet.http.HttpServletRequest;

public class NoSuchCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        return JspPageName.ERROR_PAGE;
    }
}
