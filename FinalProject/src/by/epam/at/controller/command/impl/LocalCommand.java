package by.epam.at.controller.command.impl;

import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LocalCommand implements Command {

    /**
     * Changes attribute local in the session scope.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page from request.
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session;
        if ((session = request.getSession(false)) == null)
        {
            session = request.getSession(true);
        }

        session.setAttribute(ParameterName.LOCAL, request.getParameter(ParameterName.LOCAL));
        return request.getParameter(ParameterName.PAGE);
    }
}
