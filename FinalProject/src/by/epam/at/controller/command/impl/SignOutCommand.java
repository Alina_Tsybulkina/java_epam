package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import javax.servlet.http.HttpServletRequest;

public class SignOutCommand implements Command {

    /**
     * Invalidate current session.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of index page.
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession(false).invalidate();
        return JspPageName.INDEX_PAGE;
    }
}
