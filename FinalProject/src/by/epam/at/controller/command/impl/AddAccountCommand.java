package by.epam.at.controller.command.impl;

import by.epam.at.controller.JspPageName;
import by.epam.at.controller.ParameterName;
import by.epam.at.controller.command.Command;
import by.epam.at.controller.command.CommandException;
import by.epam.at.entity.Account;
import by.epam.at.service.AccountsService;
import by.epam.at.service.exception.ServiceException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AddAccountCommand implements Command {

    /**
     * Method which adds bank account to user.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @return name of jsp page.
     * @throws CommandException if adding was unsuccessful.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Account account = new Account();
        account.setIdUser((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
        List<Account> accounts;
        try {
            AccountsService.getInstance().addAccount(account);
            accounts = AccountsService.getInstance().getAccountsByUserID((Integer) request.getSession(false).getAttribute(ParameterName.USER_ID));
            request.getSession(false).setAttribute(ParameterName.USER_ACCOUNTS, accounts);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return JspPageName.USER_ACCOUNTS;
    }
}
