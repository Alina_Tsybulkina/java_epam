package by.epam.at.controller;

/**
 * Stores names of jsp pages.
 */
public final class JspPageName {

    public static final String ERROR_PAGE = "error.jsp";
    public static final String INDEX_PAGE = "index.jsp";
    public static final String USER_PAGE = "WEB-INF/jsp/user.jsp";
    public static final String ADMIN_PAGE = "WEB-INF/jsp/admin.jsp";
    public static final String REGISTRATION_PAGE = "WEB-INF/jsp/registr.jsp";
    public static final String USER_PAYMENTS = "WEB-INF/jsp/user_payments.jsp";
    public static final String ADD_PAYMENT = "WEB-INF/jsp/add_payment.jsp";
    public static final String USER_ACCOUNTS = "WEB-INF/jsp/user_accounts.jsp";
    public static final String MY_PAGE = "WEB-INF/jsp/mypage.jsp";
    public static final String USER_CARDS = "WEB-INF/jsp/user_cards.jsp";
    public static final String ADD_CARD = "WEB-INF/jsp/add_card.jsp";
    public static final String ALL_PAYMENTS = "WEB-INF/jsp/admin/all_payments.jsp";
    public static final String ALL_ACCOUNTS = "WEB-INF/jsp/admin/all_accounts.jsp";
    public static final String ALL_CARDS = "WEB-INF/jsp/admin/all_cards.jsp";
    public static final String ALL_USERS = "WEB-INF/jsp/admin/all_users.jsp";
    public static final String ADD_ADMIN = "WEB-INF/jsp/admin/add_admin.jsp";
    public static final String ADD_MONEY = "WEB-INF/jsp/add_money.jsp";


    private JspPageName() {
    }
}