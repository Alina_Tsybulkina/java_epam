package by.epam.at.dao;

import java.util.List;
import java.util.Map;

public interface DAO<E> {

    E selectOne(E e) throws DaoException;
    List<E> selectByUserID(int id) throws DaoException;
    List<E> selectAll() throws DaoException;
    List<E> select(Map<String,String> map) throws DaoException;
    int insert(E obj) throws DaoException;
    int update(E e) throws DaoException;
    void delete(int id) throws DaoException;
}
