package by.epam.at.dao.impl;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DaoException;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.Account;
import com.sun.istack.internal.Nullable;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AccountsDAO<E> implements DAO<E> {
    private static final Logger LOGGER = Logger.getLogger(AccountsDAO.class);

    private static final AccountsDAO instance = new AccountsDAO();

    private static final String SELECT_ACCOUNTS_BY_ID = "SELECT * from accounts where id_user = ?";

    private static final String SELECT_ACCOUNT = "SELECT * from accounts where id_bank_account = ?";

    private static final String SELECT_ACCOUNT_BY_CARD = "SELECT a.id_bank_account, a.balance, a.is_blocked, a.id_user FROM accounts a " +
            "join cards c on c.id_bank_account = a.id_bank_account where c.id_card = ? AND a.id_user = ?";

    private static final String UPDATE_ACCOUNT = "update accounts" +
            " set balance = ?, is_blocked=?" +
            " where id_bank_account = ?";

    private static final String INSERT_ACCOUNT = "insert into accounts (balance, is_blocked, id_user) values(?, ?, ?)";

    private static final String SELECT_ALL = "select * from accounts";

    private static final String DELETE = "delete from accounts where id_bank_account = ?";

    private Connection connection;
    private PreparedStatement statement;
    private ResultSet resultSet;
    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    private AccountsDAO() {
    }

    public static AccountsDAO getInstance() {
        return instance;
    }

    /**
     * Method which selects one bank account from database by account id.
     * @param e object of class {@code Account}.
     * @return object of class {@code Account} with all fields filled in.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    @Nullable
    public E selectOne(E e) throws DaoException {
        Account account = (Account) e;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ACCOUNT);
            statement.setInt(1, account.getIdBankAccount());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                makeAccount(resultSet, account);
            } else {
                return null;
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
            try {
                statement.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
        }
        return (E) account;
    }

    /**
     * Selects list of bank accounts by user id.
     * @param id - unique identifier of user.
     * @return list of bank accounts.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectByUserID(int id) throws DaoException {
        List<Account> accounts = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ACCOUNTS_BY_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                makeAccount(resultSet, account);
                accounts.add(account);
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) accounts;
    }

    /**
     * Selects all bank accounts.
     * @return list of all bank accounts.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectAll() throws DaoException {
        List<Account> accounts = new ArrayList<>();

        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ALL);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                makeAccount(resultSet, account);
                accounts.add(account);
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) accounts;
    }

    /**
     * Selects bank account for selected user and card.
     * @param map which contains card id and user id.
     * @return list, which contains bank account for selected user and card.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> select(Map<String, String> map) throws DaoException {
        int idCard = Integer.parseInt(map.get(CardsDAO.CardsCols.ID_CARD));
        int idUser = Integer.parseInt(map.get(ACCOUNT_COLS.ID_USER));
        List<Account> accounts = new ArrayList<>(1);
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ACCOUNT_BY_CARD);
            statement.setInt(1, idCard);
            statement.setInt(2, idUser);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Account account = new Account();
                makeAccount(resultSet, account);
                accounts.add(account);
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
            try {
                statement.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
        }
        return (List<E>) accounts;
    }


    /**
     * Insert new bank account to database.
     * @param obj object of class {@code Account}.
     * @return generated key for inserted object.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public int insert(E obj) throws DaoException {
        Account account = (Account) obj;
        int key = 0;
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(INSERT_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, account.getBalance());
            statement.setBoolean(2, account.getIsBlocked());
            statement.setInt(3, account.getIdUser());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
            {
                key = resultSet.getInt(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException(e);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return key;
    }

    /**
     * Updates given bank account.
     * @param obj object of class {@Account}.
     * @return key of updated row.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public int update(E obj) throws DaoException {
        Account account = (Account) obj;
        int key = 0;
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(UPDATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, account.getBalance());
            statement.setBoolean(2, account.getIsBlocked());
            statement.setInt(3, account.getIdBankAccount());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
            {
                key = resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return key;
    }

    /**
     * Deletes bank account by id.
     * @param id of bank account.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public void delete(int id) throws DaoException {
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        }
        catch (ConnectionPoolException | SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }

    private void makeAccount(ResultSet resultSet, Account account) throws SQLException {
        account.setIdBankAccount(resultSet.getInt(ACCOUNT_COLS.ID_BANK_ACCOUNT));
        account.setBalance(resultSet.getInt(ACCOUNT_COLS.BALANCE));
        account.setIsBlocked(resultSet.getBoolean(ACCOUNT_COLS.IS_BLOCKED));
        account.setIdUser(resultSet.getInt(ACCOUNT_COLS.ID_USER));
    }

    public final static class ACCOUNT_COLS {
        public static final String ID_BANK_ACCOUNT = "id_bank_account";
        public static final String BALANCE = "balance";
        public static final String IS_BLOCKED = "is_blocked";
        public static final String ID_USER = "id_user";
    }
}
