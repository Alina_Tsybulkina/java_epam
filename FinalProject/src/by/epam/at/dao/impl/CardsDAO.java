package by.epam.at.dao.impl;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DaoException;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.Card;
import com.sun.istack.internal.Nullable;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CardsDAO<E> implements DAO<E> {

    private static final CardsDAO instance = new CardsDAO();

    private final static Logger LOGGER = Logger.getLogger(CardsDAO.class);

    private static final String SELECT_CARDS_USER_ID = "SELECT c.id_card, c.id_bank_account" +
            " FROM cards c left join accounts a" +
            " on c.id_bank_account = a.id_bank_account where id_user = ?";

    private static final String INSERT_CARD = "insert into cards (id_bank_account) values (?)";

    private static final String DELETE_CARD = "delete from cards where id_card = ?";

    private static final String SELECT_ALL = "SELECT * FROM cards";

    private static final String SELECT_CARD = "SELECT * FROM cards where id_card = ?";


    private Connection connection;
    private PreparedStatement statement;
    private ResultSet resultSet;
    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    public static CardsDAO getInstance() {
        return instance;
    }


    /**
     * Select one card by card id.
     * @param e object of class {@code Card}.
     * @return object of class {@code Card} with all fields filled in.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    @Nullable
    public E selectOne(E e) throws DaoException {
        Card card = (Card) e;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_CARD);
            statement.setInt(1, card.getIdCard());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                setCard(card, resultSet);
            } else {
                return null;
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
            try {
                statement.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
        }
        return (E) card;
    }

    /**
     * Selects all cards of selected user.
     * @param id identifier of user.
     * @return list of all cards for user.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectByUserID(int id) throws DaoException {
        List<Card> cards = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_CARDS_USER_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Card card = new Card();
                setCard(card, resultSet);
                cards.add(card);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } catch (ConnectionPoolException e) {
            throw new DaoException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) cards;
    }


    /**
     * Selects all cards.
     * @return list of all cards.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectAll() throws DaoException {
        List<Card> cards = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ALL);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Card card = new Card();
                setCard(card, resultSet);
                cards.add(card);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } catch (ConnectionPoolException e) {
            throw new DaoException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) cards;
    }

    @Override
    @Nullable
    public List<E> select(Map<String, String> map) throws DaoException {
        return null;
    }

    /**
     * Inserts object to database.
     * @param obj object of class {@code Card}.
     * @return id of inserted row.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public int insert(E obj) throws DaoException {
        Card card = (Card) obj;
        int key = 0;
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(INSERT_CARD, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, card.getIdBankAccount());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
            {
                key = resultSet.getInt(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException(e);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return key;
    }

    @Override
    public int update(E obj) throws DaoException {
        return 0;
    }

    /**
     * Deletes the card by id.
     * @param id identifier of card.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public void delete(int id) throws DaoException {
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(DELETE_CARD);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }


    private void setCard(Card card, ResultSet resultSet) throws SQLException {
        card.setIdCard(resultSet.getInt(CardsCols.ID_CARD));
        card.setIdBankAccount(resultSet.getInt(CardsCols.ID_BANK_ACCOUNT));
    }

    public final static class CardsCols {
        public static final String ID_CARD = "id_card";
        public static final String ID_BANK_ACCOUNT = "id_bank_account";
    }
}
