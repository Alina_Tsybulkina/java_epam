package by.epam.at.dao.impl;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DaoException;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.Payment;
import com.sun.istack.internal.Nullable;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PaymentsDAO<E> implements DAO<E> {
    private static final PaymentsDAO instance = new PaymentsDAO();

    private final static Logger LOGGER = Logger.getLogger(PaymentsDAO.class);

    private static final String SELECT_PAYMENTS_USER_ID = "select p.id_payment, p.id_card, p.id_bank_account_target, p.sum " +
            "from (payments p left join cards c on p.id_card = c.id_card) " +
            "left join accounts a on c.id_bank_account = a.id_bank_account " +
            "where a.id_user = ?";

    private static final String SELECT_ACCOUNT_BY_CARD = "SELECT is_blocked, a.id_bank_account FROM accounts a left join cards c " +
            "on a.id_bank_account = c.id_bank_account where c.id_card = ?";

    private static final String UPDATE_ACCOUNT = "update accounts set balance = balance + ? where id_bank_account = ?";

    private static final String INSERT_PAYMENT = "insert into payments (id_card, id_bank_account_target, sum) values (?, ?, ?)";

    private static final String SELECT_ALL = "select * from payments";

    private static final String SELECT_PAYMENT = "select * from payments where id_payment = ?";

    private static final String DELETE = "delete from payments where id_payment = ?";


    private Connection connection;
    private PreparedStatement statement;
    private ResultSet resultSet;
    private ConnectionPool connectionPool = ConnectionPool.getInstance();


    private PaymentsDAO() {
    }

    public static PaymentsDAO getInstance() {
        return instance;
    }


    /**
     * Selects payment by id.
     * @param e object of class {@code Payment}.
     * @return object of class {@code Payment} with all fields filled in.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    @Nullable
    public E selectOne(E e) throws DaoException {
        Payment payment = (Payment) e;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_PAYMENT);
            statement.setInt(1, payment.getIdPayment());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                setPayment(resultSet, payment);
            } else {
                return null;
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
            try {
                statement.close();
            } catch (SQLException ex) {
                LOGGER.error(ex);
            }
        }
        return (E) payment;
    }


    /**
     * Selects all payments for selected user.
     * @param id user identifier.
     * @return list of payment for user.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectByUserID(int id) throws DaoException {
        List<Payment> paymentsList = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_PAYMENTS_USER_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Payment payment = new Payment();
                setPayment(resultSet, payment);
                paymentsList.add(payment);
            }

        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) paymentsList;
    }

    /**
     * Inserts payment to database.
     * @param obj object of class {@code Payment}.
     * @return generated key for inserted row.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public int insert(E obj) throws DaoException {
        int key;
        Payment payment = (Payment) obj;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ACCOUNT_BY_CARD);
            statement.setInt(1, payment.getIdCard());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                if (!resultSet.getBoolean(PAYMENT_COLS.IS_BLOCKED)) {
                    key = makeTransaction(connection, payment, resultSet.getInt(PAYMENT_COLS.ID_BANK_ACCOUNT));
                } else {
                    throw new DaoException("Account is blocked!");
                }
            } else {
                throw new DaoException("No such account");
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } catch (ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return key;
    }

    /**
     * Deletes payment by id.
     * @param id identifier of payment.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public void delete(int id) throws DaoException {
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }

    /**
     * Selects all payments.
     * @return list of all payments.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectAll() throws DaoException {
        List<Payment> payments = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ALL);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Payment payment = new Payment();
                setPayment(resultSet, payment);
                payments.add(payment);
            }
        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) payments;
    }

    @Override
    @Nullable
    public List<E> select(Map<String, String> map) throws DaoException {
        return null;
    }


    @Override
    public int update(E obj) throws DaoException {
        return 0;
    }

    private void setPayment(ResultSet resultSet, Payment payment) throws SQLException {
        payment.setIdPayment(resultSet.getInt(PAYMENT_COLS.ID_PAYMENT));
        payment.setIdCard(resultSet.getInt(PAYMENT_COLS.ID_CARD));
        payment.setIdBankAccountTarget(resultSet.getInt(PAYMENT_COLS.ID_BANK_ACCOUNT_TARGET));
        payment.setSum(resultSet.getInt(PAYMENT_COLS.SUM));
    }

    private int makeTransaction(Connection connection, Payment payment, int bank_account_source) throws DaoException {
        int key = 0;
        try {
            connection.setAutoCommit(false);
            PreparedStatement statement = connection.prepareStatement(UPDATE_ACCOUNT);
            statement.setInt(1, (payment.getSum() * (-1)));
            statement.setInt(2, bank_account_source);
            statement.executeUpdate();
            statement.setInt(1, payment.getSum());
            statement.setInt(2, payment.getIdBankAccountTarget());
            statement.executeUpdate();
            statement = connection.prepareStatement(INSERT_PAYMENT, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, payment.getIdCard());
            statement.setInt(2, payment.getIdBankAccountTarget());
            statement.setInt(3, payment.getSum());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
            {
                key = resultSet.getInt(1);
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new DaoException("Rollback error!");
            }
            throw new DaoException("Transaction error!");
        }
        return key;
    }

    private final static class PAYMENT_COLS {
        public static final String ID_BANK_ACCOUNT = "id_bank_account";
        public static final String IS_BLOCKED = "is_blocked";
        public static final String ID_PAYMENT = "id_payment";
        public static final String ID_CARD = "id_card";
        public static final String ID_BANK_ACCOUNT_TARGET = "id_bank_account_target";
        public static final String SUM = "sum";
    }
}
