package by.epam.at.dao.impl;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DaoException;
import by.epam.at.dao.pool.ConnectionPool;
import by.epam.at.dao.pool.ConnectionPoolException;
import by.epam.at.entity.User;
import com.sun.istack.internal.Nullable;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserDAO<E> implements DAO<E> {
    private static final UserDAO instance = new UserDAO();

    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);

    private static final String SELECT_QUERY = "SELECT * FROM users where login = ? and password = ?";

    private static final String SELECT_LOGIN = "SELECT * FROM users where login = ?";

    private static final String INSERT_QUERY = "INSERT INTO users(name, surname, login, password, is_admin) VALUES(?,?,?,?,?)";

    private static final String UPDATE_QUERY = "update users" +
            " set name =?, surname=?, login=?, password=?" +
            " where id_user = ?";

    private static final String SELECT_ALL = "SELECT * FROM users";

    private static final String DELETE = "delete from users where id_user = ?";


    private Connection connection;
    private PreparedStatement statement;
    private ResultSet resultSet;
    private ConnectionPool connectionPool =  ConnectionPool.getInstance();

    private UserDAO() {

    }

    public static UserDAO getInstance() {
        return instance;
    }


    /**
     * Selects user by login and password.
     * @param obj object of class {@code User}.
     * @return object of class {@code User} with all fields filled in.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    @Nullable
    public E selectOne(E obj) throws DaoException {
        User user = (User)obj;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_QUERY);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                setUser(resultSet, user);
            }
            else {
                return null;
            }
        }
        catch (ConnectionPoolException|SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            }
            catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (E) user;
    }

    @Override
    @Nullable
    public List<E> selectByUserID(int id) throws DaoException {
        return null;
    }

    /**
     * Inserts user to database.
     * @param obj - object of class {@code User}
     * @return generated id for inserted row.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public int insert(E obj) throws DaoException {
        User user = (User)obj;
        int key = 0;
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getPassword());
            statement.setBoolean(5, user.getIsAdmin());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
            {
                key = resultSet.getInt(1);
            }
        }
        catch (ConnectionPoolException | SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            }
            catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return key;
    }

    /**
     * Selects all users.
     * @return list of all users.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> selectAll() throws DaoException {
        List<User> users = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_ALL);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                setUser(resultSet, user);
                users.add(user);
            }
        }
        catch (ConnectionPoolException|SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            }
            catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) users;
    }

    /**
     * Select user by login.
     * @param map contains user login.
     * @return list which contains user with such login.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public List<E> select(Map<String, String> map) throws DaoException {
      List<User> users = new ArrayList<>();
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SELECT_LOGIN);
            statement.setString(1, map.get(UserCols.LOGIN));
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                setUser(resultSet, user);
                users.add(user);
            }
        }
        catch (ConnectionPoolException|SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
            try {
                statement.close();
            }
            catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return (List<E>) users;
    }


    /**
     * Updates user data.
     * @param obj - object of class {@code User}.
     * @return id of updated row.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public int update(E obj) throws DaoException {
        User user = (User) obj;
        int key = 0;
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(UPDATE_QUERY, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getPassword());
            statement.setInt(5, user.getIdUser());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
            {
                key = resultSet.getInt(1);
            }
        }
        catch (ConnectionPoolException | SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return key;
    }

    /**
     * Deletes user by id.
     * @param id - identifier of user.
     * @throws DaoException if request to database was unsuccessful.
     */
    @Override
    public void delete(int id) throws DaoException {
        try {
            connection = connectionPool.takeConnection();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        }
        catch (ConnectionPoolException | SQLException ex)
        {
            throw new DaoException(ex);
        }
        finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }

    private void setUser (ResultSet resultSet, User user) throws SQLException {
        user.setIdUser(resultSet.getInt(UserCols.ID_USER));
        user.setName(resultSet.getString(UserCols.NAME));
        user.setSurname(resultSet.getString(UserCols.SURNAME));
        user.setLogin(resultSet.getString(UserCols.LOGIN));
        user.setPassword(resultSet.getString(UserCols.PASSWORD));
        user.setIsAdmin(resultSet.getBoolean(UserCols.IS_ADMIN));
    }


    public final static class UserCols {
        public static final String ID_USER = "id_user";
        public static final String NAME = "name";
        public static final String SURNAME = "surname";
        public static final String LOGIN = "login";
        public static final String PASSWORD = "password";
        public static final String IS_ADMIN = "is_admin";
    }
}
