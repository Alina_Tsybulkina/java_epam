package by.epam.at.dao;

import by.epam.at.dao.impl.AccountsDAO;
import by.epam.at.dao.impl.CardsDAO;
import by.epam.at.dao.impl.PaymentsDAO;
import by.epam.at.dao.impl.UserDAO;

public class DAOFactory {
	private final static DAOFactory instance = new DAOFactory();

	public static DAOFactory getInstance()
	{
		return instance;
	}

    public DAO getDAO(DAOType dao) throws DaoException {
		switch (dao)
		{
			case USER_DAO:
				return UserDAO.getInstance();
			case PAYMENTS_DAO:
				return PaymentsDAO.getInstance();
			case ACCOUNTS_DAO:
				return AccountsDAO.getInstance();
			case CARDS_DAO:
				return CardsDAO.getInstance();
			default:
				throw new DaoException("No such DAO");
		}
    }

	public enum DAOType
	{
		USER_DAO, PAYMENTS_DAO, ACCOUNTS_DAO, CARDS_DAO
	}


}
