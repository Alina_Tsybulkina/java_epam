package by.epam.at.service;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DAOFactory;
import by.epam.at.dao.DaoException;
import by.epam.at.entity.User;
import by.epam.at.service.exception.ServiceException;
import java.util.List;
import java.util.Map;

public class UserService {

    private static final UserService instance = new UserService();

    public static UserService getInstance() {
        return instance;
    }

    /**
     * Registration of user.
     * @param user new user, object of class {@code User}.
     * @throws ServiceException if registration was unsuccessful.
     */
    public void registerUser(User user) throws ServiceException {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.USER_DAO);
            dao.insert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


    /**
     * Selects user by login and password.
     * @param user object of class {@code User} with filled login and password.
     * @return result user, with all fields filled in.
     * @throws ServiceException if getting was unsuccessful.
     */
    public User getUser(User user) throws ServiceException
    {
        User resultUser;
        try {
            DAO<User> dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.USER_DAO);
            resultUser = dao.selectOne(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return resultUser;
    }

    /**
     * Updates user data.
     * @param user object of class {@code User}.
     * @throws ServiceException if updating was unsuccessful.
     */
    public void updateUser(User user) throws ServiceException
    {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.USER_DAO);
            dao.update(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Gets all users.
     * @return list of all user.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<User> selectAllUsers() throws ServiceException {
        List<User> users;
        try{
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.USER_DAO);
            users = dao.selectAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return users;
    }

    /**
     * Deletes user by id.
     * @param id user identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    public void deleteUser(int id) throws ServiceException {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.USER_DAO);
            dao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Gets user by login.
     * @param map contains login.
     * @return list of user with selected login.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<User> getByLogin(Map<String, String> map) throws ServiceException {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.USER_DAO);
            return dao.select(map);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }
}
