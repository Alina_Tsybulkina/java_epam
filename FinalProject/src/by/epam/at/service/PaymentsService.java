package by.epam.at.service;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DAOFactory;
import by.epam.at.dao.DaoException;
import by.epam.at.entity.Payment;
import by.epam.at.service.exception.ServiceException;
import java.util.List;

public class PaymentsService {

    private static final PaymentsService instance = new PaymentsService();

    public static PaymentsService getInstance() {
        return instance;
    }

    /**
     * Gets all payments for selected user.
     * @param id user identifier.
     * @return list of all payments for selected user.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<Payment> getPaymentsByUserID(int id) throws ServiceException {
        List<Payment> payments;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.PAYMENTS_DAO);
            payments = dao.selectByUserID(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

        return payments;
    }

    /**
     * Adds payment of current user.
     * @param payment object of class {@code Payment}.
     * @throws ServiceException if adding was unsuccessful.
     */
    public void addPayment(Payment payment) throws ServiceException {
        try
        {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.PAYMENTS_DAO);
            dao.insert(payment);
        }
        catch (DaoException e)
        {
            throw new ServiceException(e);
        }
    }

    /**
     * Gets all payments.
     * @return list of all payments.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<Payment> allPayments() throws ServiceException {
        List<Payment> payments;
        try
        {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.PAYMENTS_DAO);
            payments = dao.selectAll();

        }
        catch (DaoException e)
        {
            throw new ServiceException(e);
        }
        return payments;
    }
}
