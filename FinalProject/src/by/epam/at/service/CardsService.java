package by.epam.at.service;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DAOFactory;
import by.epam.at.dao.DaoException;
import by.epam.at.entity.Card;
import by.epam.at.service.exception.ServiceException;
import java.util.List;

public class CardsService {

    private static final CardsService instance = new CardsService();

    public static CardsService getInstance() {
        return instance;
    }

    /**
     * Gets all cards for selected user.
     * @param id user identifier.
     * @return list of cards for user.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<Card> getCardsByUserID(int id) throws ServiceException {
        List<Card> cards;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.CARDS_DAO);
            cards = dao.selectByUserID(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return cards;
    }

    /**
     * Deletes card by id.
     * @param id card identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    public void deleteCard(int id) throws ServiceException {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.CARDS_DAO);
            dao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds card to current user and to selected bank account.
     * @param card object of class {@code Card}.
     * @throws ServiceException if adding was unsuccessful.
     */
    public void addCard(Card card) throws ServiceException {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.CARDS_DAO);
            dao.insert(card);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Gets all cards.
     * @return list of all cards.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<Card> getAllCards() throws ServiceException {
        List<Card> cards;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.CARDS_DAO);
            cards = dao.selectAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return cards;
    }
}
