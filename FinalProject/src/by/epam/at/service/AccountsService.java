package by.epam.at.service;

import by.epam.at.dao.DAO;
import by.epam.at.dao.DAOFactory;
import by.epam.at.dao.DaoException;
import by.epam.at.entity.Account;
import by.epam.at.service.exception.ServiceException;
import com.sun.istack.internal.Nullable;
import java.util.HashMap;
import java.util.List;

public class AccountsService {

    private static final AccountsService instance = new AccountsService();

    public static AccountsService getInstance() {
        return instance;
    }

    /**
     * Receives list of bank accounts for selected user.
     * @param id user.
     * @return list of bank accounts for selected user.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<Account> getAccountsByUserID(int id) throws ServiceException {
        List<Account> accounts;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.ACCOUNTS_DAO);
            accounts = dao.selectByUserID(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return accounts;
    }

    /**
     * Selects bank account for selected user and card.
     * @param map which contains card id and user id.
     * @return null if not such accounts, and account if it exist.
     * @throws ServiceException if getting was unsuccessful.
     */
    @Nullable
    public Account getAccountByCard(HashMap<String, String> map) throws ServiceException {
        List<Account> accounts;
        Account account;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.ACCOUNTS_DAO);
            accounts = dao.select(map);
            if (accounts.isEmpty())
            {
                return null;
            }
            account = accounts.get(0);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return account;
    }

    /**
     * Method which selects one bank account by account id.
     * @param account object of class {@code Account}.
     * @return object of class {@code Account} with all fields filled in.
     * @throws ServiceException if getting was unsuccessful.
     */
    public Account getAccount(Account account) throws ServiceException {
        Account accountResult;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.ACCOUNTS_DAO);
            accountResult = (Account) dao.selectOne(account);
        }catch (DaoException e) {
            throw new ServiceException(e);
        }
        return accountResult;
    }


    /**
     * Updates given bank account.
     * @param account object of class {@Account}.
     * @throws ServiceException if updating was unsuccessful.
     */
    public void updateAccount(Account account) throws ServiceException{
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.ACCOUNTS_DAO);
            dao.update(account);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds new bank account.
     * @param account object of class {@Account}.
     * @throws ServiceException if adding was unsuccessful.
     */
    public void addAccount(Account account) throws ServiceException {
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.ACCOUNTS_DAO);
            dao.insert(account);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Gets all bank accounts.
     * @return list of all bank accounts.
     * @throws ServiceException if getting was unsuccessful.
     */
    public List<Account> getAllAccounts() throws ServiceException {
        List<Account> accounts;
        try {
            DAO dao = DAOFactory.getInstance().getDAO(DAOFactory.DAOType.ACCOUNTS_DAO);
            accounts = dao.selectAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return accounts;
    }
}
