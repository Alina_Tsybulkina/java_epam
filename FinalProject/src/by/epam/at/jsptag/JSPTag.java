package by.epam.at.jsptag;

import by.epam.at.entity.Payment;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

public class JSPTag extends TagSupport {
    List<Payment> payments;

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    /**
     * Method realize user-defined tag, prints to {@code JspWriter} table with payments.
     * @return SKIP BODY, the body is not processing and called {@code doEndTag()}.
     * @throws JspException if writing was unsuccessful.
     */
    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            for (Payment payment : payments)
            {
                out.write("<tr>");
                out.write("<td>");
                out.write(String.valueOf(payment.getIdPayment()));
                out.write("</td>");
                out.write("<td>");
                out.write(String.valueOf(payment.getIdCard()));
                out.write("</td>");
                out.write("<td>");
                out.write(String.valueOf(payment.getIdBankAccountTarget()));
                out.write("</td>");
                out.write("<td>");
                out.write(String.valueOf(payment.getSum()));
                out.write("</td>");
                out.write("</tr>");
            }

        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }
}
