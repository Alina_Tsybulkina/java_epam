package by.epam.at.entity;

import java.io.Serializable;

/**
 * Class which describes payment.
 */
public class Payment implements Serializable {
    static final long serialVersionUID = 1L;

    private int idPayment;
    private int idCard;
    private int idBankAccountTarget;
    private int sum;

    public int getIdPayment() {
        return idPayment;
    }

    public void setIdPayment(int idPayment) {
        this.idPayment = idPayment;
    }

    public int getIdCard() {
        return idCard;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public int getIdBankAccountTarget() {
        return idBankAccountTarget;
    }

    public void setIdBankAccountTarget(int idBankAccountTarget) {
        this.idBankAccountTarget = idBankAccountTarget;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (null == o) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;

        if (idPayment != payment.idPayment)
        {
            return false;
        }

        if (idCard != payment.idCard)
        {
            return false;
        }

        if (idBankAccountTarget != payment.idBankAccountTarget)
        {
            return false;
        }

        if (sum != payment.sum)
        {
            return false;
        }
        return true;

    }

    @Override
    public int hashCode() {
        return (int)(31* idPayment + idCard + idBankAccountTarget + sum);
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "idPayment: " + idPayment
                + ", idCard: " + idCard +
                ", idBankAccountTarget: " + idBankAccountTarget +
                ", sum: " + sum;
    }
}
