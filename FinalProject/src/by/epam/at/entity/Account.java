package by.epam.at.entity;

import java.io.Serializable;

/**
 * Class which describes bank account.
 */
public class Account implements Serializable {
    static final long serialVersionUID = 1L;

    private int idBankAccount;
    private int balance;
    private boolean isBlocked;
    private int idUser;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdBankAccount() {
        return idBankAccount;
    }

    public void setIdBankAccount(int idBankAccount) {
        this.idBankAccount = idBankAccount;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public int hashCode() {
        return  (31 * idBankAccount + balance + idUser + ((isBlocked) ? 1 : 0));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Account account = (Account) obj;

        if (idBankAccount != account.idBankAccount)
        {
            return false;
        }

        if (balance != account.balance)
        {
            return false;
        }

        if (isBlocked != account.isBlocked )
        {
            return false;
        }

        if (idUser != account.idUser)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "idBankAccount: " + idBankAccount
                + ", balance: " + balance
                + ", isBlocked: " + isBlocked
                + ", idUser: " + idUser;
    }
}
