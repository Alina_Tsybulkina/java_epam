package by.epam.at.entity;

import java.io.Serializable;

/**
 * Class which describes bank card.
 */
public class Card implements Serializable{
    static final long serialVersionUID = 1L;

    private int idCard;
    private int idBankAccount;

    public int getIdCard() {
        return idCard;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public int getIdBankAccount() {
        return idBankAccount;
    }

    public void setIdBankAccount(int idBankAccount) {
        this.idBankAccount = idBankAccount;
    }

    @Override
    public int hashCode() {
        return (int)(31 * idCard + idBankAccount);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Card card = (Card) obj;

        if(idCard != card.idCard)
        {
            return false;
        }

        if (idBankAccount != card.idBankAccount)
        {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "idCard: " + idCard
                + ", idBankAccount: " + idBankAccount;

    }
}
