package by.epam.at.entity;

import java.io.Serializable;

/**
 * Class which describes bank account.
 */
public class User implements Serializable{
    static final long serialVersionUID = 1L;

    private int idUser;
    private String name;
    private String surname;
    private String login;
    private String password;
    private boolean isAdmin;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (null == o) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        if (idUser != user.idUser) {
            return false;
        }
        if (isAdmin != user.isAdmin) {
            return false;
        }

        if (null == login)
        {
            return (login == user.login);
        } else {
            if(!login.equals(user.login))
            {
                return false;
            }
        }

        if (null == password)
        {
            return (password == user.password);
        } else {
            if(!password.equals(user.password))
            {
                return false;
            }
        }

        if (null == name)
        {
            return (name == user.name);
        } else {
            if(!name.equals(user.name))
            {
                return false;
            }
        }

        if (null == surname)
        {
            return (surname == user.surname);
        } else {
            if(!surname.equals(user.surname))
            {
                return false;
            }
        }

        return true;

    }

    @Override
    public int hashCode() {
        return (31 * idUser + name.hashCode() + surname.hashCode()
                + login.hashCode() + password.hashCode()
                +  ((isAdmin) ? 1 : 0));
    }

    @Override
    public String toString() {
        return getClass().getName() + "idUser: " + idUser + ", name: " + name +
                ", surname: " + surname + ", login: " + login + ", password: " +
                ", isAdmin: " + isAdmin;
    }
}
